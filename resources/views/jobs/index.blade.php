@extends('layouts.main')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Title</td>
                <td>Email</td>
                <td>Description</td>
            </tr>
            </thead>
            <tbody>
            @foreach($jobs as $job)
                <tr>
                    <td>{{$job->title}}</td>
                    <td>{{$job->user->email}}</td>
                    <td>{{ str_limit($job->description,100) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
@endsection