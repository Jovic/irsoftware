<?php

namespace IrSoftwareTest\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @throws \Exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldReport($exception) && class_exists(\LaraBug\LaraBug::class)) {
            app('larabug')->handle($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof ValidationException){
            return parent::render($request, $exception);
        } else {
            $code = $exception->getCode()?: 500;
            $message = $exception->getMessage()?: 'Unknown server error';

            return response()->view('jobs.error', ['message'=>$message], $code);
        }
    }
}
