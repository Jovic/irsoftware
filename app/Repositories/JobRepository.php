<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 4:16 PM
 */

namespace IrSoftwareTest\Repositories;

use IrSoftwareTest\Job;

class JobRepository extends BasicRepository
{
    /**
     * JobRepository constructor.
     * @param Job $model
     */
    public function __construct(Job $model)
    {
        parent::__construct($model);
    }

    /**
     * @return mixed
     */
    public function onlyApproved()
    {
        return $this->model::where('is_approved',true)->get();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function isSpam($id)
    {
        $job = $this->find($id);
        if(empty($job)) {
            throw new \Exception('Marking as spam new job failed!!');
        }
        $job->is_spam = true;
        $job->is_approved = false;
        if(!$job->save()) {
            throw new \Exception('Marking as spam new job failed!!');
        }
        return $job;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function isApproved($id)
    {
        $job = $this->find($id);
        if(empty($job)) {
            throw new \Exception('Approving new job failed!!');
        }
        $job->is_approved = true;
        $job->is_spam = false;
        if(!$job->save()) {
            throw new \Exception('Approving new job failed!!');
        }
        return $job;
    }

    /**
     * @param $data
     * @return Job
     * @throws \Exception
     */
    public function saveDefault($data)
    {
        $job = new Job();
        $job->user_id = $data['user_id'];
        $job->title = $data['title'];
        $job->description = $data['description'];
        if(!$job->save()) {
            throw new \Exception('Saving new job failed!!');
        }

        return $job;
    }
}