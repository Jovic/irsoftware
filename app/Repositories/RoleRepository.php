<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 4:16 PM
 */

namespace IrSoftwareTest\Repositories;

use IrSoftwareTest\Role;

class RoleRepository extends BasicRepository
{
    const ROLE_LEAD = 'lead';
    const ROLE_ADMIN = 'admin';

    /**
     * RoleRepository constructor.
     * @param Role $model
     */
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    /**
     * @return mixed
     */
    public function getLeadRole()
    {
        return $this->model::where('title',self::ROLE_LEAD)->first();
    }

    /**
     * @return mixed
     */
    public function getAdminRole()
    {
        return $this->model::where('title',self::ROLE_ADMIN)->first();
    }
}