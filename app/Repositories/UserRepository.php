<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 4:16 PM
 */

namespace IrSoftwareTest\Repositories;

use IrSoftwareTest\User;
use IrSoftwareTest\UserRole;

class UserRepository extends BasicRepository
{
    protected $role;

    /**
     * UserRepository constructor.
     * @param User $model
     * @param RoleRepository $role
     */
    public function __construct(User $model, RoleRepository $role)
    {
        parent::__construct($model);
        $this->role = $role;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        return $this->model::where('email',$email)->first();
    }

    /**
     * @param $email
     * @return User
     * @throws \Exception
     */
    public function saveNewLead($email)
    {
        $user = new User();
        $user->name = $email;
        $user->email = $email;
        // we're not registering real users, just leads for now, later if they become users they must reset password
        $user->password = bcrypt(rand(5,15));
        if(!$user->save()){
            throw new \Exception('Saving new user lead failed!!');
        }

        $role = $this->role->getLeadRole();
        if(empty($role)) {
            $user->delete();
            throw new \Exception('Saving new user role failed!!');
        }
        $user_role = new UserRole();
        $user_role->user_id = $user->id;
        $user_role->role_id = $role->id;
        if(!$user_role->save()){
            $user->delete();
            throw new \Exception('Saving new user role failed!!');
        }

        return $user;
    }

    /**
     * @return mixed
     */
    public function getAdminEmail()
    {
        return $this->role->getAdminRole()->users->first()->email;
    }
}