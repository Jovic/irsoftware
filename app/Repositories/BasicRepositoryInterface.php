<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 5:10 PM
 */

namespace IrSoftwareTest\Repositories;


interface BasicRepositoryInterface
{
    public function all();

    public function find($id);
}