<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 4:16 PM
 */

namespace IrSoftwareTest\Repositories;

use Illuminate\Database\Eloquent\Model;

class BasicRepository implements BasicRepositoryInterface
{
    protected $model;

    /**
     * BasicRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public  function all()
    {
        return $this->model::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model::find($id);
    }
}