<?php

namespace IrSoftwareTest\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use IrSoftwareTest\Job;

class JobNeedsModeration extends Mailable
{
    use Queueable, SerializesModels;

    protected $job;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.needs-moderation')
            ->with([
                'title' => $this->job->title,
                'description' => $this->job->description,
                'email' => $this->job->user->email,
                'approval_link' => url('approve').'/'.$this->job->id,
                'spam_link' => url('spam').'/'.$this->job->id
            ]);
    }
}
