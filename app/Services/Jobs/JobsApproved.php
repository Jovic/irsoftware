<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 9:20 PM
 */

namespace IrSoftwareTest\Services\Jobs;

use IrSoftwareTest\Repositories\JobRepository;
use IrSoftwareTest\Repositories\UserRepository;

class JobsApproved
{
    protected $user;
    protected $job;

    /**
     * JobsApproved constructor.
     * @param JobRepository $job
     * @param UserRepository $user
     */
    public function __construct( JobRepository $job,
                                 UserRepository $user)
    {
        $this->job = $job;
        $this->user = $user;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function save($data)
    {
        $user = $this->user->findByEmail($data['email']);
        if(empty($user)){
            throw new \Exception('Saving new job failed!!');
        }
        $data['user_id'] = $user->id;
        $job = $this->job->saveDefault($data);
        if(empty($job)) {
            throw new \Exception('Saving new job failed!!');
        }
        return $this->job->isApproved($job->id);
    }
}