<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 6:26 PM
 */

namespace IrSoftwareTest\Services\Jobs;


interface JobsServiceInterface
{
    public function save($data);
    public function approve($id);
    public function spam($id);
}