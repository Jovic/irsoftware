<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 9:19 PM
 */

namespace IrSoftwareTest\Services\Jobs;

use IrSoftwareTest\Repositories\UserRepository;

class JobsFactory
{
    protected $approved;
    protected $pending;
    protected $user;

    /**
     * JobsFactory constructor.
     * @param JobsApproved $approved
     * @param JobsPending $pending
     * @param UserRepository $user
     */
    public function __construct(JobsApproved $approved,
                JobsPending $pending,
                UserRepository $user)
    {
        $this->approved = $approved;
        $this->pending = $pending;
        $this->user = $user;
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public function save($data)
    {
        if(!empty($this->user->findByEmail($data['email'])))
        {
            return $this->approved->save($data);
        }
        else
        {
            return $this->pending->save($data);
        }
    }
}