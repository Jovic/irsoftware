<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 9:20 PM
 */

namespace IrSoftwareTest\Services\Jobs;


use IrSoftwareTest\Repositories\JobRepository;
use IrSoftwareTest\Repositories\UserRepository;
use IrSoftwareTest\Mail\JobInModeration;
use IrSoftwareTest\Mail\JobNeedsModeration;
use Illuminate\Support\Facades\Mail;

class JobsPending
{
    protected $user;
    protected $job;

    /**
     * JobsPending constructor.
     * @param UserRepository $user
     * @param JobRepository $job
     */
    public function __construct(UserRepository $user,
        JobRepository $job)
    {
        $this->user = $user;
        $this->job = $job;
    }

    /**
     * @param $data
     * @return \IrSoftwareTest\Job
     * @throws \Exception
     */
    public function save($data)
    {
        $user = $this->user->saveNewLead($data['email']);
        if(empty($user)){
            throw new \Exception('Saving new job failed!!');
        }
        $data['user_id'] = $user->id;
        $job = $this->job->saveDefault($data);
        if(empty($job)) {
            throw new \Exception('Saving new job failed!!');
        }

        Mail::to($this->user->getAdminEmail())->send(new JobInModeration());
        Mail::to($this->user->getAdminEmail())->send(new JobNeedsModeration($job));
        return $job;
    }
}