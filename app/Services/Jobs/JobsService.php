<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 6:24 PM
 */

namespace IrSoftwareTest\Services\Jobs;

use IrSoftwareTest\Repositories\JobRepository;
use IrSoftwareTest\Repositories\UserRepository;

class JobsService implements JobsServiceInterface
{
    protected $job;
    protected $user;
    protected $factory;

    /**
     * JobsService constructor.
     * @param JobRepository $job
     * @param UserRepository $user
     * @param JobsFactory $factory
     */
    public function __construct( JobRepository $job,
        UserRepository $user,
        JobsFactory $factory)
    {
        $this->job = $job;
        $this->user = $user;
        $this->factory = $factory;
    }

    /**
     * @param $data
     */
    public function save($data)
    {
        return $this->factory->save($data);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function approve($id)
    {
        return $this->job->isApproved($id);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function spam($id)
    {
        return $this->job->isSpam($id);
    }
}