<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 1:31 PM
 */

namespace IrSoftwareTest\Http\Controllers;

use IrSoftwareTest\Http\Requests\StoreJob;
use IrSoftwareTest\Repositories\BasicRepositoryInterface;
use IrSoftwareTest\Services\Jobs\JobsServiceInterface;

class JobsController extends Controller
{
    protected $job;
    protected $jobsService;

    /**
     * JobsController constructor.
     * @param BasicRepositoryInterface $job
     * @param JobsServiceInterface $jobsService
     */
    public function __construct(BasicRepositoryInterface $job,
    JobsServiceInterface $jobsService)
    {
        $this->job = $job;
        $this->jobsService = $jobsService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('jobs.index',['jobs'=>$this->job->onlyApproved()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('jobs.create');
    }

    /**
     * @param StoreJob $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreJob $request)
    {
        if(!empty($this->jobsService->save($request->all()))){
            return redirect()->action('JobsController@index')
                ->with('success','Job sent successfully');
        } else {
            return view('jobs.error',['message'=>'Saving new job failed!!!']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($id)
    {
        if(!empty($this->jobsService->approve($id))){
        return redirect()->action('JobsController@index')
            ->with('success','Job approved successfully');
        } else {
            return view('jobs.error',['message'=>'Approving new job failed!!!']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function spam($id)
    {
        if(!empty($this->jobsService->spam($id))){
        return redirect()->action('JobsController@index')
            ->with('success','Job marked as spam');
        } else {
            return view('jobs.error',['message'=>'Marking as spam of new job failed!!!']);
        }
    }
}