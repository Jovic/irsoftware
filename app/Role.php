<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/10/19
 * Time: 7:25 PM
 */

namespace IrSoftwareTest;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany('IrSoftwareTest\User', 'user_roles', 'role_id', 'user_id');
    }
}