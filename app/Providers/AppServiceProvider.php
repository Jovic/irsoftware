<?php

namespace IrSoftwareTest\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('IrSoftwareTest\Repositories\BasicRepositoryInterface','IrSoftwareTest\Repositories\JobRepository');
        $this->app->bind('IrSoftwareTest\Services\Jobs\JobsServiceInterface','IrSoftwareTest\Services\Jobs\JobsService');
    }
}
