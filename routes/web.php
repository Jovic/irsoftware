<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','JobsController@index');
Route::get('approve/{id}','JobsController@approve');
Route::get('spam/{id}','JobsController@spam');
Route::get('create','JobsController@create');
Route::post('create','JobsController@store');
