## Job Board assignment

This is hypothetical job board project with two defined user stories. First one is posting new jobs to the board. Second story is receiving an email with links to approve or mark the story as spam.
The scope of the stories didn't cover registration and login so assumptions are made that these are implemented already. 

Project is developed using Laravel 5.5 and MySql 8.0.15 with Laravel Valet as development environment. Before running php artisan migrate and php artisan seed please change email value of user with id 1 in UsersTableSeeder if neccessary. The user is set as admin and job moderation email will be sent to that email address. Mailtrap was used for development purposes.