<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'id'=>1,
            'name'=>'nenad',
            'email' => 'nenad@test.com',
            'password' =>bcrypt('secret'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'id'=>2,
            'name'=>'jovic',
            'email' => 'jovic@test.com',
            'password' =>bcrypt('secret'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'id'=>3,
            'name'=>'nenadjovic',
            'email' => 'nenadjovic@test.com',
            'password' =>bcrypt('secret'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'id'=>4,
            'name'=>'spam',
            'email' => 'spam@spammer.com',
            'password' =>bcrypt('secret'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],]);
    }
}
