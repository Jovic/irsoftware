<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'id'=>1,
            'title'=>'admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'id'=>2,
            'title'=>'lead',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'id'=>3,
            'title'=>'spammer',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]]);
    }
}
