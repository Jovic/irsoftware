<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([[
            'user_id'=>2,
            'title' => 'Software Developer',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sem neque, sagittis eu rhoncus at, accumsan sit amet quam. Fusce feugiat, purus non semper mollis, magna magna mollis neque, ac pretium elit metus et sem. Ut eget malesuada urna. Phasellus nec tortor sit amet magna blandit vestibulum eget facilisis elit. Proin a dui molestie elit maximus pretium. Morbi at arcu vitae augue dignissim finibus. Curabitur sed rutrum velit.',
            'is_approved' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'user_id'=>2,
            'title' => 'Project Manager',
            'description' => 'Vivamus non consequat magna. Pellentesque non dictum elit, sit amet gravida mi. Mauris tempus sem at velit rutrum dapibus. Nunc libero velit, molestie eget hendrerit eget, pulvinar ac quam. Suspendisse potenti. Donec finibus ante in purus posuere, sed viverra dui luctus. Maecenas bibendum, lorem eu placerat tincidunt, erat orci posuere risus, eu condimentum purus est at eros. Nullam euismod magna ac felis pellentesque interdum.',
            'is_approved' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ],[
            'user_id'=>3,
            'title' => 'QA Engineer',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque nec tellus faucibus, hendrerit dui vitae, imperdiet felis. Phasellus sollicitudin nibh id neque ultrices convallis. Proin porttitor libero nec nisi tempor, vitae vulputate felis pharetra. Sed rhoncus commodo neque, vel rutrum ex bibendum eleifend. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus a mauris quis tortor porta aliquet et vel turpis.',
            'is_approved' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]]);
        DB::table('jobs')->insert([
            'user_id'=>4,
            'title' => 'Spam',
            'description' => 'Spam',
            'is_spam' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
